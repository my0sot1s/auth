FROM golang:alpine AS builder
# Copy the code from the host and compile it
ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

COPY . /go/src/gitlab.com/my0sot1s/auth/
WORKDIR /go/src/gitlab.com/my0sot1s/auth/
RUN  CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o app .
# RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

# Stage 2 (to create a downsized "container executable", ~7MB)

# If you need SSL certificates for HTTPS, replace `FROM SCRATCH` with:
#
#   FROM alpine:3.7
#   RUN apk --no-cache add ca-certificates
#

FROM alpine:3.8
# RUN mkdir /app
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/my0sot1s/auth/token/id_rsa.pub id_rsa.pub
COPY --from=builder /go/src/gitlab.com/my0sot1s/auth/token/id_rsa id_rsa

RUN ["chmod","+r","id_rsa"]
RUN ["chmod","+r","id_rsa.pub"]

COPY --from=builder /go/src/gitlab.com/my0sot1s/auth/app app

EXPOSE 4323
CMD ["./app", "start"]