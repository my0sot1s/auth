package main

import (
	"testing"
	"time"

	"gitlab.com/my0sot1s/helper"
	"gitlab.com/my0sot1s/model/user"
)

func Test_coreStruct(t *testing.T) {
	u := &user.User{
		Name:    "Lan",
		Created: time.Now().UnixNano(),
	}
	// user.Name = ""
	u.Created = 0
	helper.Log(u)
	helper.Log(user.User_UserState_name[0])
}
