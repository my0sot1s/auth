package token

import (
	"testing"
	"time"

	"gitlab.com/my0sot1s/helper"
)

func Test_genToken(t *testing.T) {
	tkHelper := &TokenHelper{}
	if err := tkHelper.ChargePrivatekey("id_rsa"); err != nil {
		panic(err)
	}
	tk, err := tkHelper.Generate(helper.M{"uid": "nguyenngocngan"}, 30*time.Minute)
	t.Log(tk, err)
	helper.Log(tk, err)
}
func Test_parseToken(t *testing.T) {
	tk := "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTYwMzQ2ODMsImlhdCI6MTU1NjAzMjg4MywicGF5bG9hZCI6eyJ1aWQiOiJuZ3V5ZW5uZ29jbmdhbiJ9fQ.r74Khi1R9PXm-emdL7rUE3cIeUSUj7FvQSTq1I7IQ7XbbhNEkSh6jtXidFv62GXvADL-W28Xvt1WDOzI7cJSftfKBL4zGuChPTvVwiOOwwaVmasR_WFUJcUP3F3GFtnOC2m9ov3C3nLndS6u0LeQjFPdoMjTaQp28pc68ubZQnc"
	tkHelper := &TokenHelper{}
	if err := tkHelper.ChargePublicKey("id_rsa.pub"); err != nil {
		panic(err)
	}
	payload, err := tkHelper.ParseToken(tk)
	t.Log(payload, err)
}
