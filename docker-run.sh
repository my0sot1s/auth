#!/bin/bash
docker run --rm -d \
 --name dev_auth \
 -e "PORT=4323" \
 -e "DB_URL=mongodb://dev:123459te@ds121373.mlab.com:21373/chaplin" \
 -e "DB_NAME=chaplin" \
 -e "PUB_PATH=token/id_rsa.pub" \
 -e "PRIV_PATH=token/id_rsa" \
 my0sot1s/auth
echo "run done on background"