package main

import (
	"os"

	"github.com/urfave/cli"
	"gitlab.com/my0sot1s/helper"
)

func main() {
	helper.Log(helper.TitleConsole("H e l l o   a u t h "))
	if err := startApp(configApp()); err != nil {
		panic(err)
	}
}

func configApp() *cli.App {
	app := cli.NewApp()
	app.Action = func(c *cli.Context) error {
		helper.Log("Wow, Do you know my name??")
		return nil
	}
	return app
}

func startApp(app *cli.App) error {
	app.Commands = []cli.Command{
		{
			Name: "start",
			Action: func(ctx *cli.Context) {
				InitGrpc(":" + os.Getenv("PORT"))
				// if err := InitGrpc(":" + os.Getenv("PORT")); err != nil {
				// panic(err)
				// }
			},
		},
	}
	return app.Run(os.Args)
}
