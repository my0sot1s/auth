package main

import (
	"context"
	"errors"
	"time"

	"gitlab.com/my0sot1s/helper"

	"gitlab.com/my0sot1s/model/user"
)

// Authen core processing
type Authen struct {
	db    IDB
	tkmgr ITkMgr
	pwmgr IPwMgr
}

// LoginOauth is a core func
func (a *Authen) LoginOauth(ctx context.Context, in *user.OauthMessage) (*user.Token, error) {
	return nil, nil
}

// SignIn is a core func
func (a *Authen) SignIn(ctx context.Context, in *user.User) (*user.Token, error) {
	// check on db
	if in.GetPassword() == "" {
		return nil, errors.New("password empty")
	}
	userRequest := &user.UsersRequest{Name: in.GetName(), Email: in.GetEmail()}
	u, err := a.db.GetUserNameOrEmail(userRequest)
	if err != nil {
		return nil, err
	}
	helper.Log("u", u)
	if err := a.pwmgr.Compare(u.GetPassword(), in.GetPassword()); err != nil {
		return nil, err
	}

	tk, err := a.tkmgr.Generate(helper.MS{
		"uid": u.GetId(),
	}, 15*time.Minute)
	helper.Log("token, error", tk, err)
	return &user.Token{Token: tk}, nil
}

// SignUp is a core func
func (a *Authen) SignUp(ctx context.Context, in *user.User) (*user.User, error) {
	// check on db
	in.State = user.User_UserState_name[2]
	if len(in.GetPassword()) < 6 {
		return nil, errors.New("password invalid")
	}
	userRequest := &user.UsersRequest{Name: in.GetName(), Email: in.GetEmail()}
	if u, err := a.db.GetUserNameOrEmail(userRequest); err == nil && u.GetId() != "" {
		return nil, errors.New("user is existed")
	}
	if hashPw, err := a.pwmgr.Generate(in.GetPassword()); err == nil {
		in.Password = hashPw
	}
	in.Created = time.Now().UnixNano()
	// insert db
	return a.db.UpsertUser(in)
}

// ResetPassword is a core func
func (a *Authen) ResetPassword(ctx context.Context, in *user.User) (*user.User, error) {
	// get oldpw
	return nil, nil
}

// UpdateUser is a core func
func (a *Authen) UpdateUser(ctx context.Context, in *user.User) (*user.User, error) {
	// just update none "Name", "Password", "Created", "State", "Groups", "GroupIds":
	if in.GetId() == "" {
		return nil, errors.New("user id empty")
	}
	in.Password = ""
	in.Created = 0
	in.State = ""
	in.GroupIds = nil
	in.Name = ""
	return a.db.UpsertUser(in)
}

// ChangeState is a core func
func (a *Authen) ChangeState(ctx context.Context, in *user.User) (*user.User, error) {
	if in.GetId() == "" {
		return nil, errors.New("user id empty")
	}
	if in.GetState() == "" {
		return nil, errors.New("state is empty")
	}
	if user.User_UserState_value[in.GetState()] == 0 && user.User_UserState_name[0] != in.GetState() {
		return nil, errors.New("state is invalid")
	}
	return a.db.UpsertUser(&user.User{Id: in.GetId(), State: in.GetState()})
}

// UpsertUserToGroup is a core func
func (a *Authen) UpsertUserToGroup(ctx context.Context, in *user.UsersRequest) (*user.User, error) {
	// only add group
	if in.GetId() == "" {
		return nil, errors.New("user id empty")
	}
	return a.db.UpsertUser(&user.User{Id: in.GetId(), GroupIds: in.GetGroupIds()})
}

// ReadUsers is a core func
func (a *Authen) ReadUsers(ctx context.Context, in *user.UsersRequest) (*user.UsersResponse, error) {
	req, anchor := &user.UsersRequest{Limit: -100}, ""
	if in.GetLimit() != 0 {
		req.Limit = in.Limit
	}
	users, err := a.db.GetUsers(req)
	helper.Log("MOT", users, req)
	if err != nil {
		return nil, err
	}
	if len(users) == 0 {
		anchor = ""
	} else if req.GetLimit() > 0 {
		anchor = users[len(users)-1].GetId()
	} else {
		anchor = users[0].GetId()
	}
	return &user.UsersResponse{
		Anchor: anchor,
		Users:  users,
	}, nil
}

// ReadUser is a core func
func (a *Authen) ReadUser(ctx context.Context, in *user.UsersRequest) (*user.User, error) {
	if in.GetId() != "" {
		return a.db.GetUser(&user.UsersRequest{Id: in.GetId()})
	} else if in.GetEmail() != "" {
		return a.db.GetUserNameOrEmail(&user.UsersRequest{Email: in.GetEmail()})
	} else if in.GetName() != "" {
		return a.db.GetUserNameOrEmail(&user.UsersRequest{Name: in.GetName()})
	}
	return nil, errors.New("no field to search")
}

// ListGroups is a core func
func (a *Authen) ListGroups(ctx context.Context, in *user.GroupUserRequest) (*user.GroupUsersResponse, error) {
	req, anchor := &user.GroupUserRequest{Limit: -100}, ""
	if in.GetLimit() != 0 {
		req.Limit = in.Limit
	}
	groups, err := a.db.GetGroups(req)
	if err != nil {
		return nil, err
	}
	if len(groups) == 0 {
		anchor = ""
	} else if req.GetLimit() > 0 {
		anchor = groups[len(groups)-1].GetId()
	} else {
		anchor = groups[0].GetId()
	}
	return &user.GroupUsersResponse{
		Anchor: anchor,
		Groups: groups,
	}, nil
}

// UpsertGroup is a core func
func (a *Authen) UpsertGroup(ctx context.Context, in *user.GroupUser) (*user.GroupUser, error) {
	if in.GetId() == "" { //insert
		in.State = user.GroupUser_GroupState_name[0]
		in.Created = time.Now().UnixNano()
	} else {
		in.Created = 0
		in.State = ""
	}
	return a.db.UpsertUserGroup(in)
}

// DeleteGroup is a core func
func (a *Authen) DeleteGroup(ctx context.Context, in *user.GroupUser) (*user.GroupUser, error) {
	if in.GetId() == "" {
		return nil, errors.New("group id empty")
	}
	return nil, a.db.DeleteUserGroup(in.GetId())
}
