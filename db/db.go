package db

import (
	"errors"

	"gitlab.com/my0sot1s/helper"
	"gitlab.com/my0sot1s/model/user"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type DB struct {
	db  *mgo.Database
	url string
}

var oh = bson.ObjectIdHex

const (
	User      = "User"
	GroupUser = "GroupUser"
)

func (d *DB) InitDB(dbName, url string) error {
	session, err := mgo.Dial(url)
	if err != nil {
		return err
	}
	session.SetMode(mgo.Monotonic, true)
	d.db = session.DB(dbName)
	return nil
}

// --------- User ----------------
func (d *DB) GetUser(query *user.UsersRequest) (*user.User, error) {
	// getOne
	if !bson.IsObjectIdHex(query.GetId()) {
		return nil, errors.New("id is not hex")
	}
	u, m := &user.User{}, make(helper.M)
	if err := d.db.C(User).FindId(bson.ObjectIdHex(query.GetId())).One(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtId(m), &u)
	u.Password = ""
	return u, err
}

func (d *DB) GetUserNameOrEmail(query *user.UsersRequest) (*user.User, error) {
	// getOne
	u, m, cond := &user.User{}, make(helper.M), make(bson.M)
	if query.GetEmail() != "" {
		cond["email"] = query.GetEmail()
	} else if query.GetName() != "" {
		cond["name"] = query.GetName()
	} else {
		return nil, errors.New("no email or name")
	}
	if err := d.db.C(User).Find(cond).One(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtId(m), &u)
	return u, err
}

func (d *DB) GetUsers(query *user.UsersRequest) ([]*user.User, error) {
	m, users := make([]helper.M, 0), make([]*user.User, 0)
	anchor, limit := query.GetAnchor(), int(query.GetLimit())
	helper.Log("DB", d.db)
	if err := d.db.C(User).Find(limitAnchor(limit, anchor)).Limit(limit).All(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtSliceId(m), &users)
	for _, u := range users {
		u.Password = ""
	}
	return users, err
}

func (d *DB) UpsertUser(u *user.User) (*user.User, error) {
	if !bson.IsObjectIdHex(u.GetId()) {
		m, bid := make(bson.M), bson.NewObjectId()
		helper.ConveterI2M(u, m)
		m["_id"], u.Id = bid, bid.Hex()
		return u, d.db.C(User).Insert(m)
	}
	u.Password = ""
	u.Created = 0
	u.Email = ""
	u.Name = ""
	m, bid := make(bson.M), bson.ObjectIdHex(u.GetId())
	helper.ConveterI2M(u, m)
	return u, d.db.C(User).UpdateId(bid, bson.M{"$set": m})
}

func (d *DB) DeleteUser(id string) error {
	if !bson.IsObjectIdHex(id) {
		return errors.New("id is not hex")
	}
	return d.db.C(User).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

// -------------------- group ---------------
func (d *DB) GetGroup(query *user.GroupUserRequest) (*user.GroupUser, error) {
	// getOne
	if !bson.IsObjectIdHex(query.GetId()) {
		return nil, errors.New("id is not hex")
	}
	group, m := &user.GroupUser{}, make(helper.M)
	if err := d.db.C(GroupUser).FindId(bson.ObjectIdHex(query.GetId())).One(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtId(m), &group)
	return group, err
}

func (d *DB) GetGroups(query *user.GroupUserRequest) ([]*user.GroupUser, error) {
	m, units := make([]helper.M, 0), make([]*user.GroupUser, 0)
	limit := int(query.GetLimit())
	if err := d.db.C(GroupUser).Find(limitAnchor(limit, "")).Limit(limit).All(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtSliceId(m), &units)
	return units, err
}

func (d *DB) UpsertUserGroup(u *user.GroupUser) (*user.GroupUser, error) {
	if !bson.IsObjectIdHex(u.GetId()) {
		m, bid := make(bson.M), bson.NewObjectId()
		helper.ConveterI2M(u, m)
		m["_id"], u.Id = bid, bid.String()
		return u, d.db.C(GroupUser).Insert(m)
	}
	m, bid := make(bson.M), bson.ObjectIdHex(u.GetId())
	helper.ConveterI2M(u, m)
	return u, d.db.C(GroupUser).UpdateId(bid, bson.M{"$set": m})
}

func (d *DB) DeleteUserGroup(id string) error {
	if !bson.IsObjectIdHex(id) {
		return errors.New("id is not hex")
	}
	return d.db.C(GroupUser).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}
