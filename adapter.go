package main

import (
	"net"
	"os"
	"time"

	"gitlab.com/my0sot1s/auth/db"
	"gitlab.com/my0sot1s/auth/hashing"
	"gitlab.com/my0sot1s/auth/token"
	"gitlab.com/my0sot1s/helper"
	user "gitlab.com/my0sot1s/model/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// ITkMgr define Token modules
type ITkMgr interface {
	Generate(interface{}, time.Duration) (string, error)
	ParseToken(string) (helper.M, error)
}

// IPwMgr define PW modules
type IPwMgr interface {
	Generate(string) (string, error)
	Compare(string, string) error
}

// IDB interface of db
type IDB interface {
	GetUser(*user.UsersRequest) (*user.User, error)
	GetUsers(*user.UsersRequest) ([]*user.User, error)
	GetUserNameOrEmail(*user.UsersRequest) (*user.User, error)
	UpsertUser(*user.User) (*user.User, error)
	DeleteUser(string) error
	GetGroup(*user.GroupUserRequest) (*user.GroupUser, error)
	GetGroups(*user.GroupUserRequest) ([]*user.GroupUser, error)
	UpsertUserGroup(*user.GroupUser) (*user.GroupUser, error)
	DeleteUserGroup(string) error
}

// InitTokenMgr just init
func InitTokenMgr(pubPath, privPath string) (*token.TokenHelper, error) {
	tk := &token.TokenHelper{}
	return tk, tk.InitToken(pubPath, privPath)
}

// InitCrypto just init
func InitCrypto() (*hashing.Hash, error) {
	return &hashing.Hash{}, nil
}

// InitDB just init
func InitDB(dbURL, dbName string) (*db.DB, error) {
	dbx := &db.DB{}
	dbx.InitDB(dbName, dbURL)
	return dbx, nil
}

// InitGrpc create auth server
func InitGrpc(port string) error {
	listen, err := net.Listen("tcp", port)
	if err != nil {
		return err
	}
	// make core
	db, err := InitDB(os.Getenv("DB_URL"), os.Getenv("DB_NAME"))
	if err != nil {
		return err
	}
	helper.Log("db connected: ", os.Getenv("DB_NAME"))
	tkmgr, err := InitTokenMgr(os.Getenv("PUB_PATH"), os.Getenv("PRIV_PATH"))
	if err != nil {
		return err
	}
	pwmgr, err := InitCrypto()
	if err != nil {
		return err
	}
	helper.Log("Crypto actived")
	helper.Log("Auth grpc running at ", port)
	serve := grpc.NewServer()
	user.RegisterUserStreamServer(serve, &Authen{db: db, tkmgr: tkmgr, pwmgr: pwmgr})
	reflection.Register(serve)
	return serve.Serve(listen)
}
